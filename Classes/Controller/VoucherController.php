<?php
namespace DCNGmbH\MooxPaymentVoucher\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 Dominic Martin <dm@dcn.de>, DCN GmbH
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use \TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use \TYPO3\CMS\Core\Messaging\FlashMessage;
use \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface; 
 
/**
 *
 *
 * @package moox_payment_voucher
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class VoucherController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {
	
	/**
	 * persistenceManager
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface
	 * @inject
	 */
	protected $persistenceManager;
	
	/**
	 * helperService
	 *
	 * @var \DCNGmbH\MooxPaymentVoucher\Service\HelperService
	 * @inject
	 */
	protected $helperService;
	
	/**
	 * voucherRepository
	 *
	 * @var \DCNGmbH\MooxPaymentVoucher\Domain\Repository\VoucherRepository
	 * @inject
	 */
	protected $voucherRepository;
	
	/**
	 * elements
	 *
	 * @var \array
	 */
	protected $elements;
	
	/**
	 * extConf
	 *
	 * @var boolean
	 */
	protected $extConf;
	
	/**
	 * compatibility
	 *
	 * @var boolean
	 */
	protected $compatibility;
	
	/**
	 * Path to the locallang file
	 * @var string
	 */
	const LLPATH = 'LLL:EXT:moox_payment_voucher/Resources/Private/Language/locallang_be.xlf:';
	
	/**
	 * initialize the controller
	 *
	 * @return void
	 */
	protected function initializeAction() {
		
		parent::initializeAction();					
		
		
		$this->extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['moox_payment_voucher']);	

		// initialize configuration manager
		$this->configurationManager = $this->objectManager->get('TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface');
		
		$elements = $this->configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK,"MooxPaymentVoucher")['elements'];
		$this->elements = array();
		if(is_array($elements)){			
			foreach($elements AS $key => $element){
				$label = LocalizationUtility::translate($element['label'],"");
				if(!$label){
					$label = $element['label'];
				}
				$this->elements[$key] = $label;
			}
		}
		
		if (version_compare(TYPO3_branch, '7.0', '<')) {
			$this->compatibility = true;
		}
	}
	
	/**
	 * action index
	 *
	 * @param \string $download download
	 * @return void
	 */
	public function indexAction($download = NULL) {			
		
		// append all selector to filter
		$this->elements = array_merge(array("all" =>"Alle Gutscheinziele"),$this->elements);
		
		// set download identifier
		$this->view->assign('identifier', $download);
		
		// set template variables
		$this->view->assign('elements', $this->elements);
		$this->view->assign('compatibility', $this->compatibility);		
		$this->view->assign('action', 'index');
	}
	
	/**
	 * action generate
	 *		
	 * @param array $generate generate
	 * @return void
	 */
	public function generateAction($generate = NULL) {			
		
		// init action arrays and booleans
		$messages = array();
		$errors = array();
		$codes = array();
		
		if(count($this->elements)){		
			if($generate){
				if($generate['element'] && isset($this->elements[$generate['element']]) && (int)$generate['amount']>0){
					//$voucher
					$voucherCnt = 0;
					$identifier = md5(time().$generate['element'].$generate['amount']);
					while($voucherCnt<(int)$generate['amount']){
						$code = $this->helperService->generateVoucherCode();
						$lookup = $this->voucherRepository->findByCode($code);					
						if(!is_object($lookup)){
							
							// set item values
							$item = $this->objectManager->get('DCNGmbH\MooxPaymentVoucher\Domain\Model\Voucher');
							$item->setTitle($code);
							$item->setElements($generate['element']);
							$item->setIdentifier($identifier);
																
							if((int)$this->extConf['codeExpiresAfterXDays']>0){
								$expires = mktime(0,0,0,date("n"),date("j")+(int)$this->extConf['codeExpiresAfterXDays'],date("Y"));
								$item->setEndtime($expires);
							}
							
							// save item to database
							$this->voucherRepository->add($item);								
							$this->persistenceManager->persistAll();
							
							$voucherCnt++;
						}					
					}
					
					// add message
					$messages[] = array( 
						"icon" => '<span class="glyphicon glyphicon-ok icon-alert" aria-hidden="true"></span>',
						"title" => LocalizationUtility::translate(self::LLPATH.'mod1.action_generate',$this->extensionName),
						"text" => LocalizationUtility::translate(self::LLPATH.'mod1.action_generate.success',$this->extensionName),
						"type" => FlashMessage::OK
					);
					
					// reset form
					unset($generate);
					
					// set flash messages
					//$this->helperService->setFlashMessages($this,$messages);
					
					// set download identifier
					$this->view->assign('identifier', $identifier);
					
					// redirect to index
					//$this->redirect("index",NULL,NULL,array("download"=>$identifier));				
				}
			}
		} else {
				
			// add message
			$messages[] = array( 
				"icon" => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
				"title" => LocalizationUtility::translate(self::LLPATH.'mod1.action_generate',$this->extensionName),
				"text" => LocalizationUtility::translate(self::LLPATH.'mod1.action_generate.error.no_elements',$this->extensionName),
				"type" => FlashMessage::ERROR
			);	
		}
		
		// set flash messages
		$this->helperService->setFlashMessages($this,$messages);
		
		// set template variables
		$this->view->assign('generate', $generate);
		$this->view->assign('elements', $this->elements);	
		$this->view->assign('compatibility', $this->compatibility);
		$this->view->assign('action', 'generate');
	}
	
	/**
	 * action download
	 *		
	 * @param \string $identifier identifier
	 * @return void
	 */
	public function downloadAction($identifier = NULL) {			
		
		if($identifier){
			
			// set filter
			$filter['identifier'] = $identifier;
			
			// get items			
			$items = $this->voucherRepository->findByFilter($filter,NULL,NULL,NULL,"all",array("disabled"));
			
			$csv = "code;target;expires\n";
			
			foreach($items AS $item){
				$csv .= $item->getTitle().";";
				if(isset($this->elements[$item->getElements()])){
					$label = LocalizationUtility::translate($this->elements[$item->getElements()],"");
					if(!$label){
						$label = $this->elements[$item->getElements()];
					}
				} else {
					$label = $item->getElements();
				}
				$csv .= $label.";";
				if($item->getEndtime()>0){
					$csv .= date("Y-m-d",$item->getEndtime());
				} else {
					$csv .= "never";
				}
				$csv .= "\n";				
			}
			
			header('Content-Type: application/csv');
			header('Content-Disposition: attachment; filename=voucher_codes_'.$identifier.'_'.date("YmdHis").'.csv');
			header('Pragma: no-cache');
				
			echo $csv;
				
			exit();
			
			$this->view = NULL;
			
		} else {
			
			// redirect to index
			$this->redirect("index");
		}
	}
	
	/**
	 * Returns ext conf
	 *
	 * @return array
	 */
	public function getExtConf() {
		return $this->extConf;
	}

	/**
	 * Set ext conf
	 *
	 * @param array $extConf ext conf
	 * @return void
	 */
	public function setExtConf($extConf) {
		$this->extConf = $extConf;
	}
}
?>