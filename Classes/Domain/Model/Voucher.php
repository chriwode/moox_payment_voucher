<?php
namespace DCNGmbH\MooxPaymentVoucher\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 Dominic Martin <dm@dcn.de>, DCN GmbH
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use \TYPO3\CMS\Extbase\Persistence\ObjectStorage;
use \TYPO3\CMS\Extbase\Utility\LocalizationUtility;
 
/**
 *
 *
 * @package moox_payment_voucher
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Voucher extends AbstractEntity {
	
	/**
	 * uid
	 *
	 * @var \integer
	 */
    protected $uid;
	
	/**
	 * pid
	 *
	 * @var \integer
	 */
    protected $pid;
	
	/**
	 * tstamp
	 *
	 * @var \integer
	 */
    protected $tstamp;		
	
	/**
	 * starttime
	 *
	 * @var \integer
	 */
    protected $starttime;
	
	/**
	 * endtime
	 *
	 * @var \integer
	 */
    protected $endtime;
	
	/**
	 * crdate
	 *
	 * @var \integer
	 */
    protected $crdate;
	
	/**
	 * deleted
	 *
	 * @var \integer
	 */
    protected $deleted;
	
	/**
	 * hidden
	 *
	 * @var \integer
	 */
    protected $hidden;
	
	/**
	 * fe user
	 *
	 * @var \DCNGmbH\MooxPaymentVoucher\Domain\Model\FrontendUser
	 */
	protected $feUser = NULL;
	
	/**
	 * uid foreign
	 *
	 * @var \integer
	 */
    protected $uidForeign;		
		
	/**
	 * title
	 *	
	 * @var \string
	 * @validate NotEmpty
	 */
	protected $title;
	
	/**
	 * elements
	 *
	 * @var \string	
	 */
	protected $elements;

	/**
	 * used
	 *
	 * @var \integer
	 */
    protected $used;
	
	/**
	 * identifier
	 *
	 * @var \string	
	 */
	protected $identifier;
	
	/**
     * get uid
	 *
     * @return \integer $uid uid
     */
    public function getUid() {
       return $this->uid;
    }
     
    /**
     * set uid
	 *
     * @param \integer $uid uid
	 * @return void
     */
    public function setUid($uid) {
        $this->uid = $uid;
    }
	
	/**
     * get pid
	 *
     * @return \integer $pid pid
     */
    public function getPid() {
       return $this->pid;
    }
     
    /**
     * set pid
	 *
     * @param \integer $pid pid
	 * @return void
     */
    public function setPid($pid) {
        $this->pid = $pid;
    }
	
	/**
     * get tstamp
	 *
     * @return \integer $tstamp tstamp
     */
    public function getTstamp() {
       return $this->tstamp;
    }
     
    /**
     * set tstamp
	 *
     * @param \integer $tstamp tstamp
	 * @return void
     */
    public function setTstamp($tstamp) {
        $this->tstamp = $tstamp;
    }		
	
	/**
     * get starttime
	 *
     * @return \integer $starttime starttime
     */
    public function getStarttime() {
       return $this->starttime;
    }
     
    /**
     * set starttime
	 *
     * @param \integer $starttime starttime
	 * @return void
     */
    public function setStarttime($starttime) {
        $this->starttime = $starttime;
    }
	
	/**
     * get endtime
	 *
     * @return \integer $endtime endtime
     */
    public function getEndtime() {
       return $this->endtime;
    }
     
    /**
     * set endtime
	 *
     * @param \integer $endtime endtime
	 * @return void
     */
    public function setEndtime($endtime) {
        $this->endtime = $endtime;
    }
	
	/**
     * get crdate
	 *
     * @return \integer $crdate crdate
     */
    public function getCrdate() {
       return $this->crdate;
    }
     
    /**
     * set crdate
	 *
     * @param \integer $crdate crdate
	 * @return void
     */
    public function setCrdate($crdate) {
        $this->crdate = $crdate;
    }
	
	/**
	 * Get year of crdate
	 *
	 * @return \integer
	 */
	public function getYearOfCrdate() {
		return $this->getCrdate()->format('Y');
	}

	/**
	 * Get month of crdate
	 *
	 * @return \integer
	 */
	public function getMonthOfCrdate() {
		return $this->getCrdate()->format('m');
	}

	/**
	 * Get day of crdate
	 *
	 * @return \integer
	 */
	public function getDayOfCrdate() {
		return (int)$this->crdate->format('d');
	}
	
	/**
	 * Returns the deleted
	 *
	 * @return \integer $deleted
	 */
	public function getDeleted() {
		return $this->deleted;
	}

	/**
	 * Sets the deleted
	 *
	 * @param \integer $deleted
	 * @return void
	 */
	public function setDeleted($deleted) {
		$this->deleted = $deleted;
	}	
	
	/**
	 * Returns the hidden
	 *
	 * @return \integer $hidden
	 */
	public function getHidden() {
		return $this->hidden;
	}

	/**
	 * Sets the hidden
	 *
	 * @param \integer $hidden
	 * @return void
	 */
	public function setHidden($hidden) {
		$this->hidden = $hidden;
	}	
	
	/**
	 * Returns the fe user
	 *
	 * @return \DCNGmbH\MooxPaymentVoucher\Domain\Model\FrontendUser $feUser
	 */
	public function getFeUser() {
		return $this->feUser;
	}

	/**
	 * Sets the fe user
	 *
	 * @param \DCNGmbH\MooxPaymentVoucher\Domain\Model\FrontendUser $feUser
	 * @return void
	 */
	public function setFeUser(\DCNGmbH\MooxPaymentVoucher\Domain\Model\FrontendUser $feUser) {
		$this->feUser = $feUser;
	}
	
	/**
     * get uid foreign
	 *
     * @return \integer $uidForeign uid foreign
     */
    public function getUidForeign() {
       return $this->uidForeign;
    }
     
    /**
     * set uid foreign
	 *
     * @param \integer $uidForeign uid foreign
	 * @return void
     */
    public function setUidForeign($uidForeign) {
        $this->uidForeign = $uidForeign;
    }			
	
	/**
	 * Returns the title
	 *
	 * @return \string $title
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * Sets the title
	 *
	 * @param \string $title
	 * @return void
	 */
	public function setTitle($title) {
		$this->title = $title;
	}
	
	/**
	 * Returns the elements
	 *
	 * @return \string $elements
	 */
	public function getElements() {
		return $this->elements;
	}

	/**
	 * Sets the elements
	 *
	 * @param \string $elements
	 * @return void
	 */
	public function setElements($elements) {
		$this->elements = $elements;
	}
	
	/**
     * get used
	 *
     * @return \integer $used
     */
    public function getUsed() {
       return $this->used;
    }
     
    /**
     * set used
	 *
     * @param integer $used used
	 * @return void
     */
    public function setUsed($used) {
        $this->used = $used;
    }	

	
	/**
	 * Returns the identifier
	 *
	 * @return \string $identifier
	 */
	public function getIdentifier() {
		return $this->identifier;
	}

	/**
	 * Sets the identifier
	 *
	 * @param \string $identifier
	 * @return void
	 */
	public function setIdentifier($identifier) {
		$this->identifier = $identifier;
	}
}
?>