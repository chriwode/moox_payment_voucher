<?php
namespace DCNGmbH\MooxPaymentVoucher\Service;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Dominic Martin <dm@dcn.de>, DCN GmbH
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use \TYPO3\CMS\Core\SingletonInterface;
use \TYPO3\CMS\Core\Utility\GeneralUtility;
use \TYPO3\CMS\Core\Messaging\FlashMessage;
use \TYPO3\CMS\Extbase\Utility\LocalizationUtility; 
 
/**
 *
 *
 * @package moox_payment
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class PaymentProvider implements SingletonInterface {
	
	/**
	 * objectManager
	 *
	 * @var \TYPO3\CMS\Extbase\Object\ObjectManager	
	 */
	protected $objectManager;	

	/**
	 * persistenceManager
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface	 
	 */
	protected $persistenceManager;
	
	/**
	 * frontendUserRepository
	 *
	 * @var \DCNGmbH\MooxPaymentVoucher\Domain\Repository\FrontendUserRepository	 
	 */
	protected $frontendUserRepository;
	
	/**
	 * voucherRepository
	 *
	 * @var \DCNGmbH\MooxPaymentVoucher\Domain\Repository\VoucherRepository	
	 */
	protected $voucherRepository;
	
	/**
	 * accessControlService
	 *
	 * @var \DCNGmbH\MooxPaymentVoucher\Service\AccessControlService	 
	 */
	protected $accessControlService;
	
	/**
	 * extConf
	 *
	 * @var \array
	 */
	protected $extConf;	
	
	/**
	 * Path to the locallang file
	 * @var string
	 */
	const LLPATH = 'LLL:EXT:moox_payment_voucher/Resources/Private/Language/locallang.xlf:';
	
	/**
     *
     * @return void
     */
    public function initialize() {								
		
		// initialize object manager
		$this->objectManager = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');		
		
		// initialize persistence manager
		$this->persistenceManager = $this->objectManager->get('TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface');
		
		// initialize frontend user repository
		$this->frontendUserRepository = $this->objectManager->get('DCNGmbH\MooxPaymentVoucher\Domain\Repository\FrontendUserRepository');
		
		// initialize voucher repository
		$this->voucherRepository = $this->objectManager->get('DCNGmbH\MooxPaymentVoucher\Domain\Repository\VoucherRepository');
		
		// initialize access control service
		$this->accessControlService = $this->objectManager->get('DCNGmbH\MooxPaymentVoucher\Service\AccessControlService');
				
		// get extensions's configuration
		$this->extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['moox_payment_voucher']);
				
    }	
	
	/**
	 * get payment configuration
	 *	
	 * @return \array $paymentConfig
	 */
	public function getPaymentConfig(){
		
		// initialize
		$this->initialize();
		$paymentConfig = array();
		
		
		$paymentConfig['extkey'] 	= "moox_payment_voucher";
		$paymentConfig['extkeyUCC'] = GeneralUtility::underscoredToUpperCamelCase($paymentConfig['extkey']);
		$paymentConfig['llpath'] 	= self::LLPATH;
		$paymentConfig['title'] 	= LocalizationUtility::translate(self::LLPATH.'payment.title',$this->extensionName);
		$paymentConfig['tab'] 		= LocalizationUtility::translate(self::LLPATH.'payment.tab',$this->extensionName);
		$paymentConfig['partial'] 	= "Voucher";
		
		return $paymentConfig;
	}

	/**
	 * check payment configuration
	 *	
	 * @param \array $payment payment
	 * @param \array &$messages messages
	 * @param \array &$errors errors
	 * @return \boolean $paid
	 */
	public function checkPayment($payment = array(), &$messages, &$errors){
		
		$paid = true;
		
		if($payment['voucher_code']==""){
			
			// add message
			$messages[] = array( 
				"icon" => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
				"title" => LocalizationUtility::translate(self::LLPATH.'payment.voucher_code',$this->extensionName),
				"text" => LocalizationUtility::translate(self::LLPATH.'payment.voucher_code.errors.empty',$this->extensionName),
				"type" => FlashMessage::ERROR
			);
			
			// set error
			$errors['payment']['moox_payment_voucher']['voucher_code'] = true;			
			$paid = false;
			
		} else {
			
			$voucher = $this->voucherRepository->findByCode($payment['voucher_code']);
			
			if(is_object($voucher)){
				
				if($voucher->getTitle()!=$payment['voucher_code'] || $voucher->getHidden()==1 || $voucher->getDeleted()==1){
					
					// add message
					$messages[] = array( 
						"icon" => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
						"title" => LocalizationUtility::translate(self::LLPATH.'payment.voucher_code',$this->extensionName),
						"text" => LocalizationUtility::translate(self::LLPATH.'payment.voucher_code.errors.invalid',$this->extensionName),
						"type" => FlashMessage::ERROR
					);
				
					// set error
					$errors['payment']['moox_payment_voucher']['voucher_code'] = true;
					$paid = false;
					
				} elseif($voucher->getUsed()>0){
					
					// add message
					$messages[] = array( 
						"icon" => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
						"title" => LocalizationUtility::translate(self::LLPATH.'payment.voucher_code',$this->extensionName),
						"text" => LocalizationUtility::translate(self::LLPATH.'payment.voucher_code.errors.already_used',$this->extensionName),
						"type" => FlashMessage::ERROR
					);
				
					// set error
					$errors['payment']['moox_payment_voucher']['voucher_code'] = true;
					$paid = false;
					
				} elseif($voucher->getEndtime()>0 && $voucher->getEndtime()<time()){
					
					// add message
					$messages[] = array( 
						"icon" => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
						"title" => LocalizationUtility::translate(self::LLPATH.'payment.voucher_code',$this->extensionName),
						"text" => LocalizationUtility::translate(self::LLPATH.'payment.voucher_code.errors.expired',$this->extensionName),
						"type" => FlashMessage::ERROR
					);
				
					// set error
					$errors['payment']['moox_payment_voucher']['voucher_code'] = true;
					$paid = false;
					
				} elseif($voucher->getElements()!=$payment['voucher_target']){
					
					// add message
					$messages[] = array( 
						"icon" => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
						"title" => LocalizationUtility::translate(self::LLPATH.'payment.voucher_code',$this->extensionName),
						"text" => LocalizationUtility::translate(self::LLPATH.'payment.voucher_code.errors.wrong_target',$this->extensionName),
						"type" => FlashMessage::ERROR
					);
				
					// set error
					$errors['payment']['moox_payment_voucher']['voucher_code'] = true;
					$paid = false;
				}
				
				if($paid){
					
					// check and get logged in user
					if(TRUE === $this->accessControlService->hasLoggedInFrontendUser()) {
						$feUser = $this->frontendUserRepository->findByUid($this->accessControlService->getFrontendUserUid());		  
					}	
					
					// set voucher as used
					$voucher->setUsed(time());
					if((int)$payment['voucher_target_uid']>0){
						$voucher->setUidForeign((int)$payment['voucher_target_uid']);
					}
					if(is_object($feUser)){
						$voucher->setFeUser($feUser);
					}
					
					// save item to database
					$this->voucherRepository->add($voucher);								
					$this->persistenceManager->persistAll();
				}
				
			} else {
				
				// add message
				$messages[] = array( 
					"icon" => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
					"title" => LocalizationUtility::translate(self::LLPATH.'payment.voucher_code',$this->extensionName),
					"text" => LocalizationUtility::translate(self::LLPATH.'payment.voucher_code.errors.invalid',$this->extensionName),
					"type" => FlashMessage::ERROR
				);
			
				// set error
				$errors['payment']['moox_payment_voucher']['voucher_code'] = true;
				$paid = false;
			}
		}
		
		return $paid;
		
	}
}
?>