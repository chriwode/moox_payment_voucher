<?php
namespace DCNGmbH\MooxPaymentVoucher\Service;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 Dominic Martin <dm@dcn.de>, DCN GmbH
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use \TYPO3\CMS\Core\SingletonInterface;
use \TYPO3\CMS\Core\Utility\GeneralUtility;
use \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use \TYPO3\CMS\Core\Messaging\FlashMessage;
use \TYPO3\CMS\Extbase\Utility\LocalizationUtility; 
 
/**
 *
 *
 * @package moox_payment_voucher
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class HelperService implements SingletonInterface {
	
	/**
	 * objectManager
	 *
	 * @var \TYPO3\CMS\Extbase\Object\ObjectManager	
	 */
	protected $objectManager;
	
	/**
	 * configurationManager
	 *
	 * @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface	
	 */
	protected $configurationManager;
		
	/**
	 * configuration
	 *
	 * @var \array	
	 */
	protected $configuration;
	
	/**
	 * extConf
	 *
	 * @var boolean
	 */
	protected $extConf;
	
	/**
	 * storagePids
	 *
	 * @var array 	
	 */
	protected $storagePids;
	
	/**
	 * Path to the locallang file
	 * @var string
	 */
	const LLPATH = 'LLL:EXT:moox_payment_voucher/Resources/Private/Language/locallang.xlf:';
	
	/**
     *
     * @return void
     */
    public function initialize() {								
		
		// initialize object manager
		$this->objectManager = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');		
		
		// initialize configuration manager
		$this->configurationManager = $this->objectManager->get('TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface');
				
		// get typoscript configuration
		$this->configuration = $this->configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK,"MooxPaymentVoucher");		
		
		// get extensions's configuration
		$this->extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['moox_payment_voucher']);
				
    }	
		
	/**
	 * set flash messages
	 *
	 * @param \mixed &$parent
	 * @param \array $messages
	 * @return void
	 */
	public function setFlashMessages(&$parent = NULL, $messages = array()) {				
		
		if($parent){
		
			// set flash messages
			foreach($messages AS $message){
				if(!is_array($message)){
					$message = array();
				}
				if($message['text']==""){
					$message['text'] = "Unbekannter Fehler / Unknown error";
				}				
				if($message['icon']!="" && $message['title']!=""){
					$message['title'] = $message['icon'].$message['title'];
				}
				$parent->addFlashMessage($message['text'],($message['title']!="")?$message['title'].": ":"",$message['type'],true);				
			}
		}
	}
	
	/**
	 * Generate voucher code
	 *
	 * @return	\string	code 	
	 */
	public function generateVoucherCode(){
		
		// initialize
		$this->initialize();
		
		$code = "";
		$allowedChars = $this->extConf['allowedChars'];
		$charCount = strlen($allowedChars);
		
		if($charCount>$this->extConf['codeLength']){
			for ($i=0; $i<$this->extConf['codeLength']; $i++) {
				$code .= $allowedChars[rand(0,$charCount-1)];
			}
		}
		return $code;		
	}		
	
	/**
	 * Returns storage pids
	 *
	 * @return \array
	 */
	public function getStoragePids() {
		return $this->storagePids;
	}

	/**
	 * Set storage pids
	 *
	 * @param \array $storagePids storage pids
	 * @return void
	 */
	public function setStoragePids($storagePids) {
		$this->storagePids = $storagePids;
	}
}
?>