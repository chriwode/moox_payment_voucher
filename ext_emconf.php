<?php
########################################################################
# Extension Manager/Repository config file for ext "moox_payment_voucher".
#
# Manual updates:
# Only the data in the array - everything else is removed by next
# writing. "version" and "dependencies" must not be touched!
########################################################################

$EM_CONF[$_EXTKEY] = array(
    'title' => 'MOOX Payment Voucher',
    'description' => 'Bezahlung per Gutschein-Code',
    'category' => 'plugin',
    'author' => 'Dominic Martin',
    'author_email' => 'dm@dcn.de',
    'shy' => '',
    'dependencies' => '',
    'conflicts' => '',
    'priority' => 'bottom',
    'module' => '',
    'state' => 'beta',
    'internal' => '',
    'uploadfolder' => 0,
    'createDirs' => '',
    'modify_tables' => '',
    'clearCacheOnLoad' => 1,
    'lockType' => '',
    'author_company' => 'DCN GmbH',
    'version' => '7.0.0',
    'constraints' => array(
        'depends' => array(
            'typo3' => '6.2.3-6.2.99',
        ),
        'conflicts' => array(),
        'suggests' => array(),
    )
);
