<?php
// Set language source file
$ll = 'LLL:EXT:moox_payment_voucher/Resources/Private/Language/locallang.xlf:';

return array(
	'ctrl' => array(
		'title' => $ll.'form.voucher',
		'label' => 'title',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'sortby' => 'sorting',
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,
		'origUid' => 't3_origuid',		
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),		
		'searchFields' => 'title',		
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('moox_payment_voucher').'Resources/Public/Icons/tx_mooxpaymentvoucher_domain_model_voucher.gif',
		'hideTable' => FALSE
	),
	'interface' => array(
		'showRecordFieldList' => 'hidden, title, elements, used, identifier',
	),
	'types' => array(
		'1' => array('showitem' => 'title, elements, used,--div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access,hidden, starttime, endtime'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(		
		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'starttime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'fe_user' => array(			
			'exclude' => 0,
			'label' => $ll.'form.fe_user',
			'config' => array(
				'type' => 'select',
				'allowNonIdValues' => 1,
				'default' => '',
				'foreign_table' => 'fe_users',
				'foreign_table_where' => 'ORDER BY fe_users.last_name',
        		'size' => 1,				
				'maxitems' => 1,
				'minitems' => 0,
				'multiple' => 0,        		
				'items' => array(
					array('Keine Auswahl', ''),
				),								
			),			
		),
		'uid_foreign' => array(
			'label' => 'uid_foreign',
			'config' => array(
				'type' => 'passthrough'
			),			
		),
		'title' => array(
			'exclude' => 0,
			'label' => $ll.'form.title',
			'config' => array(
				'type' => 'input',
				'size' => 40,
				'eval' => 'required,trim'
			),
		),		
		'elements' => array(
			'exclude' => 0,
			'label' => $ll.'form.elements',
			'config' => array(
				'type' => 'select',
				'size' => 1,
				'minitems' => 1,
				'maxitems' => 1,
				'itemsProcFunc' => 'DCNGmbH\MooxPaymentVoucher\Hooks\TcaFormHelper->elements'
			),
		),
		'used' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => $ll.'form.used',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,				
			),			
		),	
		'identifier' => array(
			'label' => 'identifier',
			'config' => array(
				'type' => 'passthrough'
			),			
		),
	),
);
?>